Hey, Alter! Dokumentation
=========================

Willkommen zur (technischen) Dokumentation des Hey, Alter! Projektes.
Im Sinne vom Open Source-Gedanken ist diese Dokumentation unter der [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)-Lizenz
lizensiert. Demnach verzichten alle an der Dokumentation beteiligten Personen
auf ihr Urheberrecht und andere Schutzrechte, soweit dies möglich ist.

Navigation
----------

* [Allgemeine Dokumentation](global/README.md)
* Standortabhängige Dokumentation
  * [Braunschweig](bs/README.md)
  * [Solingen](solingen/README.md)
  * [Wülfrath](wuelfrath/README.md)
* [Hardware / Peripherie Empfehlungen](hardware/README.md)
* Über diese Dokumentation
  * [Wie kann ich an dieser Dokumentation mitarbeiten?](CONTRIBUTING.md)
  * [Standards und Empfehlungen für den Stil](STYLE.md)

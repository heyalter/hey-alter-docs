WLAN-Sticks Empfehlungen
========================


| WLAN-Stick                                             | Status        | Links                                                                                           |
| ------------------------------------------------------ | ------------- | ----------------------------------------------------------------------------------------------- |
| TP-Link TL-WN823N WLAN USB Stick (300Mbit/s 2,4 GHz)   | funktioniert  | [Spezifikationen](https://www.tp-link.com/de/home-networking/adapter/tl-wn823n/#specifications) |

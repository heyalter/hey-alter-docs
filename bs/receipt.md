Annahme
=======

Neu angekommene Laptops werden von dem Annahmepersonal mit einem kleinen Stand (Stehtisch + Stuhl + Utensilien) von den Spendern entgegengenommen.
Dabei muss jeder Spender einen entsprechenden Annahmebogen ausfüllen (ist bei den Standutensilien dabei, sonst digital im Datenpaket).
Der Annahmebogen wird zu den gespendeten Rechnern gelegt. Teilweise wird dann der entsprechenden Person, die die Nummerierung durchführt, noch mitgeteilt, welcher Bogen zu welchem PC gehört.

Am Ende der Annahmezeit bzw. ggf. schon zwischendurch bringt die Annahmestelle die bereits erhaltenen Rechner über den Fahrstuhl hoch ins Hey, Büro, wo dann die [Inventarisierung](inventory.md) stattfindet.


TODO
----
Was nehmen wir an, was nehmen wir nicht an?

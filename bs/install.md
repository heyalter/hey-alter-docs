Installation
============

**Sollte bei der Installation irgendwelche Probleme bzw. komisches Verhalten auftreten, die ihr nicht
lösen könnt, dann bitte einen Post-It-Note mit der Problembeschreibung, aktuellem Zustand und ggf. Namen
auf den Rechner kleben und sich einem anderen Rechner zuwenden**


Installation von Towern/SFF
---------------------------

* WLAN-Karte einbauen
  * Bei SFF nicht immer möglich, dann WLAN-USB-Adapter ins Gerät stecken
* SSD einbauen (HDD nicht ausbauen)
  * Bei SFF nicht immer möglich, dann SSD bevorzugen und HDD ausbauen
  * SATA-Kabel von eingebauten HDDs abziehen
* Entstauben/Grundgereinigen
  * **aktuell noch nicht möglich**, daher nach Aufsetzen auf den Gerätestapel zum Enstauben im Lagerraum links vorm Fenster stellen
* PC an entsprechenden Arbeitsplatz anschließen
  * Ein Displaykabel anschließen
    * abhängig davon, was für Anschlüsse bereitstehen
    * Bei mehreren Möglichkeiten den besten Anschluss wählen (DP -> HDMI -> DVI -> VGA)
    * Bsp. PC mit DVI und VGA => DVI nutzen
  * Falls DVI oder VGA genutzt wird => Audioklinkenkabel anschließen
  * Maus und Tastatur per USB anschließen
    * Wenn Maus an Tastatur angeschlossen ist, muss die Maus nicht an den PC angeschlossen werden
  * USB-Stick mit schwarzem Kabelbinder an einen USB 3-Port anschließen
    * Falls es keinen USB 3-Port gibt, irgeneinen USB-Port nutzen
  * Kaltgerätekabel bzw. Netzteil anschließen
* PC starten und das BIOS öffnen
  * SATA-Mode im BIOS auf "AHCI" stellen
* BIOS Einstellungen speichern, PC neustarten und PC übers Bootmenü vom USB-Stick starten
* Das System installiert sich von selbst
  * Bei der Aufforderung den USB-Stick abzuziehen, die SATA-Kabel wieder an die HDDs anschließen
  * USB-Stick abziehen und PC mit Enter neustarten
* Einloggen mit Passwort `schule`
* `setup.sh` auf dem Desktop ausführen
  * Hier wird nochmal nach dem Passwort `schule` gefragt. Dies muss blind eingegeben werden und mit Enter bestätigt werden!
  * Computerspecs prüfen, mind. 4 Gb und 2 CPU-Kerne
* Vom Intenso Stick mit silbernem Klebeband die Dateien [`2update.sh`](https://gitli.stratum0.org/heyalter/image-build-legacy/-/snippets/20/raw/master/2update.sh?inline=false) und [`3addhdd.sh`](https://gitli.stratum0.org/heyalter/image-build-legacy/-/snippets/21/raw/master/3addhdd.sh?inline=false) auf den Desktop kopieren und den Stick wieder abziehen
* Terminal öffnen
  * `./Desktop/2update.sh` ausführen
  * Im geöffneten GParted auf `sdb` eine GPT-Partitionstabelle erstellen und ein ext4-Dateisystem mit der Bezeichnung `data` anlegen
  * `./Desktop/3addhdd.sh` ausführen
* [Qualitätskontrolle](quality_checklist.md)
* Ins Regal für die fertigen Rechner stellen, aber noch nicht als "fertig zur Abgabe" markieren (wir haben nicht immer gleich alles an Peripherie da, entsprechend markiert wird wenn zur Auslieferung mit Monitor etc. konfektioniert)


Installation von Notebooks
--------------------------

Sollte das Notebook zwei Festplattenslots haben (2 * 2,5" oder msata/nvme + 2,5") und es daher möglich sein HDD+SSD zu verbauen (**aber nicht 2 mal SSD**),
so bitte den ersten Abschnitt der folgenden Liste durch die Anleitung [Installation von Towern/SFF](#installation-von-towern-sff) ersetzen.

* SSD einbauen
* entstauben und Tastatur mit Druckkluft auspusten
  * Bei Lüftern aufpassen, entweder blockieren oder so pusten, dass sie nicht auf hohe Drehzahl kommen
  * (aktuell noch nicht möglich, dann nach Aufsetzen in das Regal zu den zu entstaubenden Laptops im Lagerraum rechts vom Fenster legen)
* USB Stick mit schwarzem Kabelbinder vorzugsweise in einen USB 3-Slot stecken
* System vom USB-Stick booten
  * automatische Installation abwarten
  * Stick entfernen und mit Enter bestätigen
* Einloggen mit `schule`
* `setup.sh` auf dem Desktop ausführen 
  * Hier wird nochmal nach dem Passwort `schule` gefragt. Dies muss blind eingegeben werden und mit Enter bestätigt werden!
  * Computerspecs prüfen, mind. 4 Gb und 2 CPU-Kerne
  * Webcambild sollte bei vorhandener Webcam sichtbar sein
* Vom Intenso Stick mit silbernem Klebeband die Dateien `1updateonly.sh` auf den Desktop kopieren und den Stick wieder abziehen
* Terminal öffnen
  * `./Desktop/1updateonly.sh` ausführen
* [Qualitätskontrolle](quality_checklist.md)


* Bildschirm/Gehäuse/Tasturoberseite mit Isoprop reinigen
  * ggf. vorher Aufkleber entfernen
* Blaue Karte auf Tastatur legen
  * **Nicht auf den Bildschirm kleben!**
  * Wenn auf dem Nummernsticker das Passwort steht, dann blaue Karte ohne Passwort, sonst mit Passwort
* Laptop auf mind. 60% laden, falls Akku nicht kaputt
* In fancy "Hey, Alter!"-Beutel packen
  * So packen, dass der Aufkleber mit der Nummer auf dem Geäuse möglichst nah an der Beutelöffnung ist
  * Netzteil auf den Laptop im Beutel legen
    * **Nicht am Laptop stecken lassen, das tötet die Ladebuchsen sobald die Tasche einmal falsch abgesetzt wird**
    * Kabel vernünftig aufwickeln, falls Klettband vorhanden damit zusammenbinden
  * Beutel zuziehen
* Beutel ins Regal für fertige Laptops packen
* Nummer in der Geräteliste als "fertig zur Abgabe" markieren

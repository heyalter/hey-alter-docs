Übersicht interner Planungstools
================================

Intern werden derzeitig zwei Google-Docs Dokumente genutzt. Die URLs bitte bei euren einweisenden Personen erfragen. Diese werden hier kurz beschrieben.


Schichtplan
-----------

Der Schichtplan definiert, wer wann anwesend ist. Dieser ist in zwei Teile unterteilt:
* Annahmezeiten
  * Definiert, wer an welchem Annahmetermin für die Annahme zuständig ist
  * ~~Es sollten immer zwei Personen für die Annahme eingetragen sein~~ Wegen COVID-19-Vorgaben derzeitig nicht möglich
    * Ermöglicht, dass immer eine Person an der Annahme anwesend ist
    * Bsp. notwendig, falls der Platz eng wird. Dann bringt die andere Person bereits die PCs den Fahrstuhl hoch.
* Generelle Anwesenheit
  * Jeder, der plant anwesend zu sein, sollte sich hier eintragen
  * **Wichtig, um COVID-19-Richtlinien einzuhalten, damit nicht unerwartet zu viele Personen kommen**


Geräteliste
-----------

Die Geräteliste ist ein Spreadsheet, in welchem alle (Braunschweiger-)Rechner aufgelistet sind. Es umfasst folgende Spalten:
* Rechnernummer
* Abgebende Institution / Person
* Typ
  * `Laptop`
  * `Tower`
  * `SFF` für [Small Form Factor](https://de.wikipedia.org/wiki/Small_Form_Factor)
* Hersteller
* Modell
* Kamera integriert (1 = ja, 0 = nein)
* Professionelle Löschung?
* Protokoll für die professionelle Löschung vorhanden?
* Danke E-Mail versandt?
* Spendenquittung benötigt?
* Gerät fertig zur Abgabe? (1 = ja)
* Gerät ausgeliefert? (1 = ja)
* Schule
* Kommentar
  * üblicherweise für spezielle Anmerkungen beim Setup genutzt
  * bsp. Akku defekt oder proprietären NVIDIA-Treiber installiert

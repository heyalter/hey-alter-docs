Infrastruktur
=============

Netzwerkinfrastruktur
---------------------

* Ein WLAN-Switch, der ein heyalter-WLAN aussendet mit folgenden LAN-Anschlüssen
  * Ein Notebook, welches den Uplink realisiert
  * Ein Intel-NUC als "Server"
    * Mirrort zurzeit via `apt-mirror` täglich das Ubuntu-Repository
    * hat 1 TB Festplattenplatz (Mirror braucht ~100 GB)
  * Drei frei liegende LAN-Kabel, um Tower PCs oder Notebooks mit Internet/Updates zu versorgen

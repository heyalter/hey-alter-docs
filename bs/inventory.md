Inventarisierung
================

Nach der [Annahme](receipt.md) landet alles Angenommene zunächst im Hey, Büro!


Vorsortierung
-------------

* Alles an Peripheriegeräten, was nicht speziell zu dem eigentlich Laptop/SFF/Tower/AiO/Tablet gehört, kann getrennt von diesen wegsortiert werden.
  * Insbesondere Monitore, Tastaturen, Kaltgerätekabel und Monitorkabel.
  * Aber nicht: Netzteile die zum Gerät gehören, oder Spezialkabel wie y-Kabel für die Grafikkarten in den Geräten
  * CDs und Anleitungen etc. sind Müll (bitte trennen)
* Monitore können ohne Kabel (außer es handelt sich im externe Netzteile) in einen Lagerraum gestellt werden
* Tastaturen und Mäuse und weitere Peripheriegeräte wie Webcams und Headsets können gesichtet werden und wenn sauber und nicht vergilbt in entsprechende Kartons im Lager bzw. der IT einsortiert werden.
  * Bei Funktastaturen und Funkmäusen bitte vorher den Funkempfänger aus dem Laptop entfernen und in die Maus oder Tastatur stecken. (oder ankleben)
* Kabel aufwickeln (nicht knoten!)


Nummerierung
------------

* Wenn Rechner schon sichtbar nicht den Hey-Alter Anorderungen entspricht, dann nicht nummerieren
  * Im Zweifel besser nummerieren, wird dann von der IT als B-Ware deklariert oder aussortiert
* In der Geräteliste die nächste freie Nummer raussuchen
* Die ersten Felder in der Geräteliste ausfüllen anhand des Annahmescheins und Sticker/Modellbezeichnungen auf dem Rechner
  * `Thinkpad` oder `Akoya` sind nur ein Teil der Modellbezeichnung, `T530` oder `E6416` dürfen gerne mit dazu geschrieben werden. Je genauer die Angaben sind, desto weniger aufwendig wird es, wenn es mit mit dem Gerät Probleme geben sollte. 
* Hey-Alter Sticker mit entsprechender `BS-[Nummer]` beschriften
  * **Wichtig: Sehr leserlich schreiben!** Wenn es Probleme mit den Geräten bei den SuS gibt, brauchen wir die Nummer; wenn die SuS die Nummer nicht entziffern können, können wir ihnen nicht so gut/schnell helfen.
  * Darauf achten, dass die Oberfläche sauber und staubfrei ist, sonst muss die IT nach dem Putzen einen neuen Aufkleber anbringen -> doppelter Aufwand.
  * Laptops: Auf der Rückseite vom Bildschirm, sodass die Nummer bei geöffnetem Laptop von hinten gut lesbar in einer der oberen Ecken ist
  * Tower/SFF-PCs: Nach Möglichkeit oben auf den PC aufkleben, aber **nicht über mehrere Teile/Kanten, damit der Sticker beim Auseinanderbau nicht durchgeschnitten werden muss**
* Falls der Rechner professionell gelöscht werden muss, einen roten Punkt auf den Sticker kleben und auf einen separaten Stapel stellen!
* Nummer des Rechners oben auf den Annahmezettel schreiben und diesen im Ordner mit den Annahmezetteln abheften
  * Falls mehrere Rechner über einen Annahmezettel kommen, den Nummernbereich aufschreiben, bsp. `1020-1025`, für die 6 Rechner mit den Beschriftungen `BS-1020`, `BS-1021`, `BS-1022`, `BS-1023`, `BS-1024`, `BS-1025`
* Laufzettel entsprechende zur Geräteklasse ausfüllen und mit einem kleinen Streifen Gaffatape befestigen
  * eventuelle Zusatzinfo wie bereits bekannte Defekte mit aufschreiben
* Geräte zur Profil-Löschung auf den Stapel zur Profilöschung stellen
* Laptops zusammen mit ihrem Netzteil in einen der Laptopkartons mit den Trennwänden stellen
  * Netzteilstecker nicht in den Laptop stecken, das kann beim Handling die Buchsen beschädigen
* Tower in einen der Lagerräume wegsortieren, möglichst zu ähnlichen Geräten dazu (bei HP, Dell, Fujitsu, Lenovo gibt es häufig schon ähnliche Bauarten) 


TODO
----
* Genauer Ort für Löschgeräte
* Was tun bei Entsorgung
* Unklarheiten?
  * Drucker und sonstiges?

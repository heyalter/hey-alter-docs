Braunschweig
============

Dies ist die spezifische Dokumentation für die Inititiative in Braunschweig.

* [Übersicht interner Planungstools](planning.md)
* Abläufe
  * [Annahme](receipt.md)
  * [Inventarisierung](inventory.md)
  * [Installation](install.md)
  * [Konfektionierung von Desktops/SFF](packaging.md)
* Checklisten
  * [Qualitätskontrolle](quality_checklist.md)
* [Infrastruktur](infrastructure.md)

Konfektionierung von Desktops/SFF
=================================

* Geräte nur dann zur Auslieferung konfektionieren, wenn wirklich alle Zubehörteile vorhanden sind.
* Zur Fehlerminimierung: In kleinen Batches arbeiten
  * also z.B. immer 5 (gleiche) Geräte auf einmal fertig packen, nicht 23.
  * erst alle Zubehörteile abgezählt hinlegen, dann Taschen packen, wenn was überbleibt oder zu wenig ist, muss man nur 5 Geräte/Taschen kontrollieren, nicht 23.
  * Auch wenn die Batches mal größer sind (z.B. noch eine Handvoll Sets mit blauen Karten ausgestattet werden sollen, weil vergessen), dann erst die Sets zählen, dann die Teile abzählen und dann verteilen.
* Materialliste je Tower/SFF
  * 1 Netzkabel (oder Netzteil+Netzkabel)
  * 1 Monitor je Tower/SFF mit passendem Anschluss (DVI/DP/HDMI ggü. VGA bevorzugen, gerade bei hohen Auflösungen)
    * DP -> HDMI -> DVI -> VGA
    * Displays nach Möglichkeit an den Anschluss anpassen
      * D.h. Kein Display mit DP nehmen, wenn nur DVI genutzt wird
      * Dann eher schauen, ob es Bildschirme gibt, die nur DVI (und VGA) können und diese bevorzugt nutzen
  * 1 Netzkabel für den Monitor (oder Netzteil+Netzkabel, sofern der Monitor das braucht)
  * 1 Tastatur
    * OVP+kabelgebunden bevorzugen, solange wir gerade Vorrat haben
    * wenn gebraucht: mit Lappen und Seifenwasser reinigen
  * 1 Maus
    * OVP+kabelgebunden bevorzugen, solange wir gerade Vorrat haben
    * wenn gebraucht: mit Lappen und Seifenwasser reinigen
  * 1 blaue Karte
  * 1 "Passwort: schule"-Aufkleber
    * entweder das ist auf dem HA-Gerätenummernaufkleber mit drauf (ab ca. Nr 1130 der Fall)
    * oder sollte per Aufkleber auf der Karte mit draufstehen.
  * 1 Audiokabel
    * Nur wenn DVI/VGA genutzt wird
    * Audiotest bei DP/HDMI nicht vergessen. Falls das nicht klappt auch ein Audiokabel dazu
  * 1 Headset je Tower/SFF
    * nur wenn die Geräte einen getrennten Kopfhörer+Mikrofoneingang vorne am Gehäuse haben
* Setup testen
  * Rechner+Monitor auf Aufsetztisch zusammenstöpseln und booten
  * Sound testen, ggf. Soundausgabe an Rechner/Monitor konfigurieren
  * Bei vielen gleichen Geräten/Monitoren reicht es vermutlich, wenn wir mit einem Rechner alle Monitore und dann mit einem Monitor alle Rechner testen.
* Auf den Karton der Tastatur (oder auf Aufkleber der Tastatur(, oder direkt auf die Unterseite) die Nummer des Rechners schreiben, sodass Beutel+Bildschirm später auch nach dem Transport eindeutig dem Rechner zugeordnet werden können
* Beutel so packen, dass die Tastatur hinten und Logo vorne ist, damit drückt später nichts gegen das Panel und das Logo ist vorne sichtbar.
* weiteres Zubehör/Peripherie dazupacken
* Beutel an passenden Bildschirm hängen
* Rechnernummern in der Geräteliste als "fertig zur Abgabe" markieren

Zuordnung zu einer Auslieferung/Schule
--------------------------------------
* Gruppiert zur Abholung hinstellen, Zettel mit Gerätenummern und ggf. Schulname dazu
* Gerätenummern im entsprechenden Eintrag im Auslieferungssheet ergänzen
  * Der Schritt mag wie doppelte Buchführung aussehen, ist aber wichtig, da wir häufiger schon Geräte für eine Schule gepackt haben, die dann aber nicht abgeholt wurden. Wenn dies direkt in die Geräteliste wandern würde, ist das nur mit viel Aufwand nachzuvollziehen.

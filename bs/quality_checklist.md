Qualitätskontrolle
==================

* Software
  * [ ] SATA-Modus im BIOS auf AHCI gestellt
  * [ ] PC startet problemlos Ubuntu
  * [ ] Kein WLAN-Netzwerk eingespeichert
  * [ ] WLAN-Netzwerke werden gefunden
  * [ ] Webcam funktioniert
  * [ ] Falls CD-Laufwerk vorhanden: Wird CD erkannt
  * [ ] System ist auf einer SSD-Platte installiert
  * [ ] Falls HDD installiert: Wird die automatisch gemountet und besitzt der Nutzer Schreibrechte?
  * [ ] System zeigt 4 GB oder mehr Arbeitsspeicher an
  * [ ] System hat mind. nen Dual-Core
  * [ ] Systemd Heyalter-Service ist nicht disabelt (`systemctl --user status heyalter`)
  * [ ] **Akutell:** Keine i915 Grafik verbaut
* Hardware
  * [ ] SSD eingebaut
  * [ ] WLAN-Karte eingebaut
  * [ ] Entstaubt bzw. grundgereinigt
  * [ ] Bildschirm, Gehäuse und Tastaturoberseite gereinigt
  * [ ] Keine kaputten Aufkleber
  * [ ] BIOS-Batterie voll
  * [ ] Akku ist zu > 30% geladen
  * [ ] Kabel haben keine sichtbaren Beschädigungen
* Verpackung
  * [ ] Falls Profilöschung benötigt (roter Punkt), wurde die gemacht (schwarzer Punkt in rotem Punkt)?
  * [ ] Richtige blaue Karte drinne (falls kein PW aufm Sticker, dann muss der auf der Karte stehen)
  * [ ] Rechnernummer möglichst nah an der Taschenöffnung
  * [ ] Netzteil mit im Beutel
  * [ ] Netzteil nicht am Laptop/PC angeschlossen
  * [ ] Netzteil ordentlich aufgewickelt

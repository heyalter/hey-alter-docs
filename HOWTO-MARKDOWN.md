Einführung in Markdown
======================

Markdown is eine Sprache, die eine Erstellung von formattierten Text
mittels eines einfachen Texteditors erlaubt. Dies ist eine Übersicht der
wichtigsten Syntaxelemente von Markdown.

Bedenkt, dass ihr auch in jedem Dokument der Dokumentation mittels des
`Edit`-Knopfes das Markdown des Dokuments sehen könnt, falls ihr mal etwas seht,
was in dieser Einführung nicht behandelt wurde.


Markdown-Varianten
------------------

Initial wurde Markdown von John Gruber spezifiziert: https://daringfireball.net/projects/markdown/syntax .
Über die Zeit hat Markdown an Beliebtheit gewonnen, sodass es in diversen Programmiersprachen und Programmen
implementiert und genutzt wurde.

Da diese Spezifikation jedoch stellenweise mehrdeutig war, wurde mit [CommonMark](https://commonmark.org/)
eine eindeutige Spezifikation geschaffen, damit Markdown auch möglichst immer in derselben Weise dargestellt wird.

Teilweise wurde die Markdown bzw. CommonMark-Spezifikation um weitere Möglichkeiten erweitert.
GitLab nutzt daher das [GitLab Flavored Markdown](https://gitli.stratum0.org/help/user/markdown), welches
CommonMark um einige Features erweitert. Dabei sollte beachtet werden, dass diese Features mit anderen
Markdown-Tools vermutlich nicht funktionieren werden.


Markdown orientiert sich an Paragraphen
---------------------------------------

Markdown fasst alle Zeilen bis zu einer leeren Zeile als Paragraph auf.
Die Häufigkeit von Zeilenumbrüchen ist hierbei in der Darstellung egal.

```
Dies ist ein Paragraph

Dies
ist
ein
zweiter
Paragraph

Nutzt Zeilenumbrüche am besten, um die
Zeilenlänge in der Textdatei zu begrenzen
```

Dies ist ein Paragraph

Dies
ist
ein
zweiter
Paragraph

Nutzt Zeilenumbrüche am besten, um die
Zeilenlänge in der Textdatei zu begrenzen


Fett, Kursiv und unterstrichen
------------------------------

Um einen Textteil fett darzustellen fügt ihr `**` am Anfang und am Ende des Textes
hinzu. Für kursiven Text nur `*` und für durchgestrichenen Text `~~`.

```
**Das ist wichtig!**
Jetzt wird es *kursiv*!
~~Rechner mit einer SSD bestücken~~Gibt grad keine SSDs
```

**Das ist wichtig!**
Jetzt wird es *kursiv*!
~~Rechner mit einer SSD bestücken~~Gibt grad keine SSDs


Überschriften
-------------

Für eine große Überschrift packt ihr unter den Text mit der Überschrift entsprechend viele `=`-Zeichen.
Für eine Unterüberschrift nutzt ihr `-` als Zeichen.

```
Große Überschrift
=================

Kleine Überschrift
------------------

Noch eine kleine Überschrift
----------------------------
```

Große Überschrift
=================

Kleine Überschrift
------------------

Noch eine kleine Überschrift
----------------------------


Aufzählungen
------------

Für Aufzählungen macht ihr einfach eine Liste, wo jeder Aufzählungspunkt mit `*` beginnt.
Für Unterpunkte jeweils zwei Leerzeichen pro Ebene vor dem Sternchen einrücken.
Bei nummerierten Listen einfach mit `1.` beginnen und durchnummerieren.
Mit dem GitLab Flavoured Markdown könnt ihr auch nach dem Sternchen ein `[ ]` für eine leere Checkbox
und `[X]` für eine ausgefüllte Checkbox schreiben.

```
* Meine Liste
* hat
  viele Einträge
  * Manchmal sind es auch Unterpunkte
    * Oder Unterunterpunkte
* Aber ich kann auch wieder zurück in die erste Ebene

1. Rechner aufsetzen
   1. Stick rein
   2. Einschalten
2. Rechner einpacken

* [X] Rechner
* [ ] DVI-Kabel
* [ ] Display
```

* Meine Liste
* hat
  viele Einträge
  * Manchmal sind es auch Unterpunkte
    * Oder Unterunterpunkte
* Aber ich kann auch wieder zurück in die erste Ebene

1. Rechner aufsetzen
   1. Stick rein
   2. Einschalten
2. Rechner einpacken

* [X] Rechner
* [ ] DVI-Kabel
* [ ] Display


Hyperlinks
----------

Hyperlinks zu Webseiten kann man entweder direkt einbauen oder mit eckigen Klammern den darzustellenden
Namen einklammern und direkt danach in runden Klammern den Link hinzufügen.
Um zu einer anderen Dokumentationsdatei zu verlinken, kann einfach der entsprechende Dateiname in die runden
Klammern geschrieben werden.

```
https://heyalter.com/ oder mit custom Namen [Hey, Alter! Webseite](https://heyalter.com).
Hier geht es zu den [Standards und Empfehlungen für den Stil](STYLE.md)
```

https://heyalter.com/ oder mit custom Namen [Hey, Alter! Webseite](https://heyalter.com).
Hier geht es zu den [Standards und Empfehlungen für den Stil](STYLE.md)


Bilder
------

Bilder in einen Ordner namens `.imgs` legen, der sich im selben Ordner, wie eure
Datei, die ihr editiert, liegt. Danach ist das wie ein Link, nur dass vor den eckigen
Klammern noch ein `!` angefügt wird.

```
Ältere Desktop-Rechner haben noch DDR3-RAM:
![4GB DDR3 RAM Riegel](.imgs/4gb_ddr3_ram.jpg)
```

Ältere Desktop-Rechner haben noch DDR3-RAM:
![4GB DDR3 RAM Riegel](.imgs/4gb_ddr3_ram.jpg)

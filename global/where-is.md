Wo finde ich was?
=================

Ein kleiner Überblick über die Datenquellen und deren Inhalt.


Neues Installations-ISO
-----------------------

Neues Installations-Image auf Basis von Ubuntu 20.04.1, welches öffentlich zugänglich ist.

* https://gitli.stratum0.org/heyalter/heyalter-ubuntu-iso/
* Neustes ISO als Zip verpackt: https://gitli.stratum0.org/heyalter/heyalter-ubuntu-iso/-/jobs/artifacts/master/download?job=build_iso
* Unterstüzt Erweiterung einer Standard Ubuntuinstallation um Hey-Alter Modifikationen
  * ISO auf nen Stick packen, einstecken und `setup-local.sh` ausführen
* Wird kontinuierlich verbessert


Datenpaket
----------

Bekommt man von `Moritz | Braunschweig` bei Initiativgründung

* Formulare und Druckdateien
  * Einlieferungsprotokoll
  * Tabellenvorlage Geräteliste
  * Aufkleber für Rechner
  * Passwortaufkleber
  * Grußkarten für jeden Rechner
  * Flyer- und Pressemitteilungen
* Installationsdateien
  * Altes Image auf Basis von Ubuntu 20.04
* Logos
  * in unterschiedliche Farben
  * Variationen mit Städten
* Design Guide
* Font-Datei

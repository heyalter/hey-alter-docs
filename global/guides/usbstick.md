Erstellen eines USB Sticks
==========================

Das jeweils aktuellste Image/ISO befindet sich unter: https://gitli.stratum0.org/heyalter/heyalter-ubuntu-iso/-/jobs/artifacts/master/download?job=build_iso
Der Download ist ein Zip-Archive, welches zuerst entpackt werden muss, um an die ISO-Datei zu gelangen.

Der Link zum Repository: https://gitli.stratum0.org/heyalter/heyalter-ubuntu-iso/
Hier kann man über GitLab Issues Fehler melden.

Die Diskussion zum Image und zur Doku findet im zulip-channel #Hilfe/Technik statt: https://chat.heyalter.com/#narrow/stream/9-Hilfe.2FTechnik



Windows
-------

* Für Windows sollte man Rufus verwenden
  * Download: https://rufus.ie/de_DE.html
  * Einstellungen sollten so aussehen:
  * ![img.png](rufus.png)


Mac
---
* Für Mac kann man Etcher verwenden
  * Download: https://www.balena.io/etcher/
  * Keine besonderen Einstellungen benötigt
  ![img.png](etcher.png)


Linux
-----
* Unter Linux kann dd verwendet werden
  * Das USB-Device mit "df" finden
    * `df`
  * USB-Stick unmounten (sdd1 als Beispiel)
    * `sudo umount /dev/sdd1`
  * dd zum ISO bespielen
    * `sudo dd if=heyalter.iso of=/dev/sdd bs=8M oflags=dsync status=progress`

Anleitungen
===========

Hier werden Anleitungen für diverse Themen gesammelt, die ggf. auch für andere Personen oder Standorte hilfreich sein können.


* Aufsetzen von Rechnern
  * [Erstellen eines USB Sticks](usbstick.md)
  * Installation eines Rechners
    * siehe Standort spezifische Anleitungen
  * [Rechner mit i915 Grafikkarte](i915Graphics.md)
  * [Debugging von Rechnerproblemen](debugging.md)
* Ubuntu-Pakete lokal vorhalten
  * [APT-Cacher-NG](apt-cacher-ng.md)

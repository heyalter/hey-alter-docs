Rechner mit i915 Grafikkarte
============================

**Es gibt einige Rechner, die eine i915 Grafik drin haben. Diese habe mit dem aktuellen 5.8er Kernel ein Problem.
Daher muss bei diesen Rechner der 5.4er Kernel nachinstalliert werden.**


Identifikation
--------------

* Ob ein Rechner ein Problem mit dem 5.8er Kernel hat, merkt man sehr schnell.
  * Sobald man irgendein Programm versucht zu starten, friert der Desktop ein und reagiert auf keine Mausklicks mehr.
  * Also funktiniert der Aufruf der `setup.sh` nicht mal mehr

Lösung
------

* Man drücke STRG + ALT + F3
  * damit wechselt man in den Terminal mode
* login mit User / Passwort: schule/schule
* `cd ./54KernelDebs/`
* `./changeTo54KernelOffline.sh`
* bestätigung mit Passwort: `schule`
* Wenn eine Meldung angezeigt wird: "Entfernen des Kernels abbrechen": mit `Nein` beantworten
* Anschließend startet der Rechner automatisch neu
* Nun sollte sich das `setup.sh` ausführen lassen

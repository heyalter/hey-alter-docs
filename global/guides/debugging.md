Debugging von Rechnerproblemen
==============================

[[_TOC_]]


Probleme beim Boot/BIOS
-----------------------

* BIOS-Batterie überprüfen


Erkennt USB-Stick nicht
-----------------------

* BIOS auf entsprechende Settings überprüfen
  * "boot from removable device" anschalten
  * "fastboot"/"quickboot" ausschalten


Unbekanntes BIOS-Passwort
-------------------------

https://bios-pw.org/ kann manchmal helfen.

Den Code bekommt man manchmal angezeigt, wenn man entweder nacheinander spezifische passwörter eingegeben hat (googlen) oder, wenn man einfach mehrfach ein falsches passwort eingibt

* PCs
  * vom Strom trennen und lange genug BIOS-Batterie entfernen
  * ggf. CMOS_Reset Pins auf Mainboard brücken
* Laptops
  * BIOS-Reset seltener möglich, nur wenige Geräte haben CMOS-Reset auf die Platine rausgeführt und dann meist nicht gelabelt
  * Acer
    * http://www.acer-userforum.de/threads/bios-passwort-unbekannt-loesungswege-fuer-bisher-bekannte-sonderfaelle.36388/
  * HP
    * Manchmal wird von einer HP Software (ProtectTools) ein Account im BIOS angelegt, es gibt dann zwar noch einen Adminaccount, der wird aber nicht angezeigt, weil kein Passwort gesetzt
    * Als Gast anmelden, ggf passwort für admin setzen, die anderen Benutzeraccounts löschen, dann passwort wieder löschen
    * https://support.hp.com/si-en/document/c03593792


Instabil bei Installation (bleibt stehen o.Ä.)
----------------------------------------------

* Hardwaredefekt?
  * Loses Kabel?
  * Loser RAM?
* BIOS-Batteriespannung überprüfen
* Installationsmedium kaputt?
* USB-Port kaputt?


Installiert, lässt sich dann aber nicht booten
----------------------------------------------

* `/var/log/installer/syslog` enthält einen log der Installation, der zum debugging von fehlern z.B. bei der Partitionierung oder Installation des Bootloaders hilfreich ist
* HP
  * https://ubuntuforums.org/showthread.php?t=2238714&p=13096071#post13096071



WLAN-Karte wird nicht erkannt
-----------------------------

* lspci ausführen, gucken was unter "Network Controller" steht
* https://help.ubuntu.com/community/WifiDocs/WirelessCardsSupported
* meist bcm43… (viele alte DELLs)
  * `bcmwl-kernel-source` installieren
* manchmal RTL8821CE
  * dafür `rtl8821ce-dkms` installieren
* WLAN-Treiber lässt sich nicht installieren?
  * Secureboot ausschalten


Akku lädt nur, wenn der Laptop ausgeschaltet ist. Gerät lässt sich aber sonst von Netzteil oder Akku betreiben
--------------------------------------------------------------------------------------------------------------

* Lösung für ein HP L3R22EA: Virtualisierung anschalten (srsly…)


Rechner rebootet anstatt herunterzufahren
-----------------------------------------

* Mögliche Lösung kann das Installieren von `laptop-mode-tools` sein. Für weitere mögliche Fixes siehe https://askubuntu.com/questions/132882/why-do-i-get-a-reboot-instead-of-a-shutdown
* Potentiell vorhandene USB3-Optionen im BIOS von "auto" auf "enabled" stellen hat auch schon geholfen (bei einem Lenovo)


MokListRT-Fehlermeldung beim Booten
-----------------------------------

Tritt üblicherweise auf, wenn PC kein SecureBoot unterstüzt oder ggf. SecureBoot ausgeschaltet ist. Der Shim-Bootloader ist eigentlich nur dafür da, um per Secureboot signiert zu werden und selbst mittels anderer Signaturmethoden den eigentlichen Bootloader (Grub) prüft und dann an diesen die Kontrolle übergibt. Unterstüzt das UEFI aber kein/nicht richtig SecureBoot, so gibt Shim noch eine Fehlermeldung aus, bevor es die Kontrolle übergibt.

Die Lösung ist Shim komplett zu deinstallieren (`/dev/sda` muss ggf. angepasst werden auf die Platte, wo Ubuntu installiert ist):
```bash
sudo apt purge shim
sudo grub-install /dev/sda
```

Webcam-Probleme
---------------

* Sind üblicherweise USB-Devices. Wenn eine verbaut ist, aber nicht funktioniert, überprüfe mit `lsusb` ob das System sie überhaupt sieht.
* Manchmal hilft es, `guvcview` (aus den Paketquellen) zu installieren, hier sind meist ein paar mehr Einstellungen rausgeführt, z.B. konnte eine Webcam die zunächst nur S/W anzeigte wieder auf Farbe umgestellt werden.

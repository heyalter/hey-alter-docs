APT-Cacher-NG
=============

Damit nicht jeder Rechner die gleichen Paketdateien erneut über unser LTE Datenlimit ziehen muss, haben wir auf einem Rechner „server“ (feste IP-Adresse 192.168.1.11) diesen `apt-cacher-ng` eingerichtet.


Server Installation
-------------------

```bash
sudo apt-get install apt-cacher-ng
```

Es stellte sich heraus, dass für Pakete von `security.ubuntu.com` die Cacher Config angepasst werden muss, sonst gibt es angebliche DNS-Fehler mit 500er Fehlercode:

```bash
sudo sh -c 'echo "http://security.ubuntu.com/ubuntu/" >> /etc/apt-cacher-ng/backends_ubuntu'
sudo service apt-cacher-ng restart
```

Das Web-Interface ist über http://192.168.1.11:3142/acng-report.html zu erreichen.


Client Konfiguration
--------------------

Dies ist die eigentliche Funktionsweise des `update.sh` Skriptes:

* erstelle Konfigurationsdatei `/etc/apt/apt.conf.d/00fablabcb-aptproxy`
* mit Inhalt `Acquire::http::Proxy "http://192.168.1.11:3142";`
* führe ein automatischen Upgrade aus: `sudo apt-get update && sudo apt-upgrade -y`
* entferne die Proxy-Konfigurationsdatei


Nachteil/TODO
-------------

Wenn des Skript durchgelaufen und die Proxy-Konfiguration gelöscht ist, schaltet sich wieder die übereifrige Aktualisierungsverwaltung ein und holt sich die APT Index-Dateien, an unserem Proxy dran vorbei. Dies allein könnten schon mal ganz paar MegaBytes sein, aber der Cacher ist auch ein wenig faul, darum findet gerade aktuell (13.01.21) die Aktualisierungsverwaltung ein Kernel Update (110MB), das von unserem `update.sh` leider nicht installiert/gecachet wird.

Komplettes Update Skript
------------------------
```bash
#!/bin/bash
#
# update using our fablabcb apt-cacher-ng (see http://192.168.1.11:3142/acng-report.html)
#
# version 0.0.4
#
# apt-cacher-ng config:
# sudo sh -c 'echo "http://security.ubuntu.com/ubuntu/" >> /etc/apt-cacher-ng/backends_ubuntu'
# sudo service apt-cacher-ng restart

script=$(cat <<'EOF'
PROXY_CONF_FILE=/etc/apt/apt.conf.d/00fablabcb-aptproxy

echo 'Acquire::http::Proxy "http://192.168.1.11:3142";' > $PROXY_CONF_FILE

apt-get update && apt-get upgrade -y

rm $PROXY_CONF_FILE

read -rn1 -p 'press the ANY key to exit'
EOF
)

#gnome-terminal -- 
sudo -- bash -xc "$script"
```

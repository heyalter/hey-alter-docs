Wie kann ich an dieser Dokumentation mitarbeiten?
=================================================

Schön dass du helfen möchtest, an dieser Dokumentation mitzuarbeiten.
Du brauchst auch kein Techniknerd zu sein, um helfen zu können :smirk: .
Um den Einstieg zu erleichtern ist hier eine kurze Anleitung.
Solltest du etwas nicht verstehen oder dir einen einfacheren Weg wünschen,
so kannst du dich gerne im Zulip unter `#Hilfe/Allgemein` oder `#Hilfe/Technik`
nach Hilfe fragen bzw. direkt `Sven | Braunschweig` kontaktieren.

Vorbereitung
------------

Um mithelfen zu können, brauchst du einen GitLab-Account auf dem Stratum0-GitLab-Server:
* Registriere dich via https://gitli.stratum0.org/users/sign_up
* Bestätige deine E-Mail-Adresse mit der automatisch versandten E-Mail
* Logge dich über den [Sign in / Register](https://gitli.stratum0.org/users/sign_in)-Button ein
  * Beachte dabei, dass du den Reiter `Standard` auswählst
* Konfiguriere dein Profil über ein [Klick auf dein Profilbild in der oberen Rechten Ecke => Button `Settings`](https://gitli.stratum0.org/-/profile)
  * Falls du deine E-Mail privat halten möchtest, achte darauf `Public email` auf `Do not show on profile` und `Commit email` auf `Use a private email` zu setzen
  * Beachte, dass du am Ende der Seite den grünen Knopf `Update profile settings` drückst, damit deine Änderungen auch übernommen werden
* Sollte ne gelbe Warnung auftauchen bezüglich SSH-Keys, so kannst du die getrost ignorieren
* Schalte deinen Account durch die Nachricht `register` an den Zulip-Bot `Doku-Bot` frei
  * Du kannst den Doku-Bot auch in einer beliebigen Stream-Nachricht erwähnen: [Beispiel](https://chat.heyalter.com/#narrow/stream/8-Test/topic/bots/near/3328)
  * Bei Problemen melde dich bei `Sven | Braunschweig`

Bevor du zu schreiben beginnst, solltest du dich mit Markdown vertraut machen. Markdown ist eine
Sprache, die man in einem einfachen Texteditor schreiben kann. Dabei gibt es
gewisse Regeln, die bsp. eine Formattierung von Text oder das Einfügen von Bildern
erlauben. Wenn du beispielsweise einen gewissen Textabschnitt fett formattieren
möchtest, dann fügst du am Anfang und Ende des Textabschnitts `**` ein.
Bei der Darstellung werden diese dann vom Computer automatisch entfernt und der
dazwischenliegende Text wird fett formattiert. Hier findest du eine kurze Einführung
in Markdown: [Einführung in Markdown](HOWTO-MARKDOWN.md)

Weiterhin kannst du dir die [Standards und Empfehlungen für den Stil](STYLE.md) durchlesen.

Änderungen vornehmen
--------------------

> **Beachte dass die Dokumentation unter [CCO](https://creativecommons.org/publicdomain/zero/1.0/deed.de) lizenziert ist. Dadurch verzichtest du (soweit gesetzlich möglich) auf deine Urheberrechte für deinen beigetragenen Content. Weiterhin bezeugst du auch, dass du die entsprechenden Rechte am Inhalt hast (bsp. dass du Bilder selbst gemacht hast oder der Urheber mit der Lizensierung unter CC0 einverstanden ist).**

Nachdem du Zugriff erhalten hast, kannst du in der Dokumentation die zu ändernde Stelle aufrufen und hast am Anfang vom Text
einen blauen Knopf mit der Aufschrift `Edit`. Diesen kannst du drücken, woraufhin dir
die Seite in Markdown angezeigt wird, welches du editieren kannst. Mit dem `Preview changes`-Reiter
kannst du dir die geänderte Seite anzeigen lassen.

Wenn du deine Änderungen eingefügt hast, so kannst du unter `Commit message` eine kurze Beschreibung
deiner Änderungen hinterlassen. Dies sollte möglichst gut die Veränderung beschreiben. Hier ein paar Beispiele:
* schlecht
  * `Update XYZ.md`
    * Ist die Standardnachricht im Textfeld
    * Ist sehr unpräzise, da nicht auf die eingentliche Änderunge eingegangen wird
  * `Dokumentation aktualisiert`
    * Wie oben. Es beinhaltet einfach keine neuen Informationen
* gute
  * `Rechtschreibfehler behoben`/`Kommatas eingefügt`
  * `Absatz "Vorbereitung" geschrieben`
  * `Bilder für den Laptop T500 eingefügt`

Unter `Target branch` kannst du einen beliebigen Namen außer `production` nutzen.
Du kannst auch gerne den Vorschlag vom GitLab in diesem Feld nutzen und nichts ändern.
> Es wird empfohlen, dass du einen Kürzel als Präfix verwendest, damit du nicht ungewollt
denselben Branch-Namen nutzt, den jemand anderes für seine Änderungen nutzt.

Dieser Branch-Name ermöglicht dir mehrere Änderungen zu bündeln und gemeinsam
zum Review zu senden. Nachdem deine Änderungen in die produktive Dokumentation
übernommen wurden, wird dieser Branch gelöscht. Weiterhin kannst du damit auch
mehrere Änderungen gleichzeitig im Review haben. Beispielsweise kannst du einen `Branch`
mit einer neu geschriebenen Dokuteilen einreichen und währen dieser reviewt wird auch
in einem anderen Branch Rechtschreibfehler korrigieren und diese gebündelt einreichen.
Falls dies eure einzige Änderung ist, so kannst du die Checkbox `Start a new merge request with these changes`
ankreuzen, damit automatisch die Anfrage auf die Integration der Branchänderungen
in die produktive Dokumentation erstellt wird.
Mit einem Klick auf `Commit changes` wird deine Änderung gespeichert.
> Der `production`-Branch beinhaltet die produktive Dokumentation, die spezielle Rechte benötigt,
um geändert werden zu können (jeder Reviewer hat diese Rechte). Dadurch können Neulinge problemlos
an der Dokumentation arbeiten, ohne dass die Gefahr einer Verschlechterung des
Inhaltes der Dokumentation besteht.

Falls du die Checkbox angeklickt hast, fahre mit dem [zweiten Absatz von Änderungen einbringen](#änderungen-einbringen) fort.

Du solltest nun wieder zurück auf der Ansicht mit den Dateien sein. Jedoch bist du nun auf der von
dir geänderten Version. Dies kannst du oben links an einer Auswahlliste mit dem von dir gewählten Branchnamen sehen.
Du kannst nun weitere Dateien editieren oder neue Dateien mit dem Plus rechts neben der Auswahlbox des Branchnamens
erstellen. Du kannst auch über die Auswahlliste mit dem Branchnamen wieder zurück zu dem `production`-Branch kommen,
um dir die derzeitige Version der Dokumentation anzusehen.

Änderungen einbringen
---------------------

Falls du die Checkbox nicht angeklickt hast, so kannst du die Anfrage (fachterminus `Merge Request`) auch manuell erstellen.
Dazu gehst du auf https://gitli.stratum0.org/heyalter/hey-alter-docs/-/merge_requests und klickst
auf `New merge request`. Nun fragt dich GitLab, welcher deiner Branches (`Source branch`) in welchen
Zielbranch (`Target branch`, sollte immer `production` sein) überführt werden soll. Dies bestätigst du mit einem Klick auf
`Compare branches and continue`.

Nun folgt ein Fester, wo du für deinen Merge Request einen Titel und eine Beschreibung festlegen musst.
Dies ist primär für den Reviewer relevant, damit er weiß, worum es geht.
Lasse die restlichen Felder auf ihren Standardwerten und klicke den `Submit merge request` um deinen Merge Request
zu erstellen. Ein Reviewer
schaut sich die Änderung ab und übernimmt die entweder direkt oder stellt eine Rückfrage bzw. einen Änderungswunsch.
Diese Änderungen führst du wie oben beschrieben auf dem zum Merge Request zugehörigen Branch aus.
Du brauchst keinen weiteren Merge-Request erstellen, da der Merge-Request automatisch merkt, dass sich in
dem angegebenen `Source Branch` Änderungen ergeben haben. Ggf. schreibst du noch einen kurzen Kommentar in
den Merge Request, falls der Reviewer deine Anpassungen nicht sieht.

Ausblick
--------

Glückwunsch! Du hast deine erste Änderung eingebracht! Wenn dir etwas an dieser Anleitung nicht gefallen hat
oder nicht verständlich war, so ist kannst du nun diese Anleitung mit deinem jetzigen Wissen verbessern.

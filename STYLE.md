Standards und Empfehlungen für den Stil
=======================================

Im folgenden sind Standards und Empfehlungen für den Aufbau dieser Dokumentation
aufgeführt. Ihr Ziel ist es, dass die Dokumentation in einer konsistenten Art
geschrieben ist, um Verwirrungen bei Neulingen und komplette Stilumbauten
zwischen zwei oder mehr Möglichkeiten zu vermeiden.

Ein paar dieser Vorgaben wurden bereits in einer [Editorconfig](https://editorconfig.org)-Datei
festgehalten. Ggf. benötigt euer Editor noch ein entsprechendes Plugin, um diese Datei
zu beachten: [Editorconfig-Pluginliste](https://editorconfig.org/#download).


UTF-8 Kodierung
---------------

UTF-8 ist ein Standard zur Darstellung von Zeichen als Binärwerte (Zeichenkodierung).
Die meisten Editoren sollten automatisch UTF-8 nutzen. Hier ein paar Special-Cases,
die möglicherweise dazu führen könnten, dass ihr in eurem Editor kein UTF-8 bekommnt:
* Erstellung einer neuen Datei, da hier kein existierender Text die Kodierung vorgibt => Umstellung der Standard-Zeichenkodierung
* Copy- und Paste von Text aus anderen Textdateien, die nicht UTF-8 kodiert wurden

Begründung:
* Vermutlich der am meist verbreiteste Standard
* Er umfasst eine riesige Menge an Zeichen, bsp. Smileys
* Er ist ASCII kompatibel


Kein HTML
---------

Die Markdown Spezifikation erlaubt zwar HTML, jedoch sollte auf einen Einsatz von HTML
verzichtet werden. Ein Einsatz von HTML für bereits durch Markdown bereitgestellte Formattierungsfeatures
ist nicht erlaubt!

Begründung:
* HTML führt dazu, dass der komplette Absatz als HTML angesehen wird, welches zu unerwarteten Problemen führen kann
* Vermeidung verschiedener (leicht unterschiedlicher) Darstellung durch die Einschränkung auf nur Markdown-Features
* Neulinge müssen nur Markdown verstehen


Bilddateien
-----------

Bilder müssen sinnvoll benannt in einen Ordner namens `.imgs` abgelegt werden.
Dieser Ordner muss sich im selben Ordner befinden, wie das Markdown-Dokument, welches das Bild nutzt.
Sollte das Bild in Markdown-Dateien von unterschiedlichen Ordnern benötigt werden, so muss es dupliziert werden.
Die Bilddateien sollten eine Größe von 500 KB nicht überschreiten.

Begründung:
* Große Bilddateien sind häufig nicht für das Verständnis notwendig
* Duplikation von Bilddateien kostet das unterliegende Git-System kaum zusätzlichen Speicherplatz
* Duplikation verhindert, dass das Umbenennen von Ordnern Bildreferenzen beschädigt
* Ordner sorgen für eine bessere Übersicht. Der Punkt-Präfix sorgt dafür, dass die auf Linux-Systemen automatisch nicht in der Dateiliste auftauchen


Überschriften
-------------

Für das erste Überschriften-Level muss die Darstellung mit `=` unter der Überschrift erfolgen.
Für das zweite Level die Darstellung mit `-` unter der Überschrift.
Bei diesen beiden Leveln ist darauf zu achten, dass die Zeichen bündig mit der Überschriftenlänge sind.

Überschriften tieferer Level sollten nur sparsam eingesetzt werden. Sie können auf eine überfüllte Seite hindeuten,
die besser in unterschiedliche Seiten aufgeteilt werden kann.

Positiv Beispiel:
```
Titel der Seite
===============

Subtitel
--------

### Noch ein tieferes Level
```

Negativ Beispiel:
```
# Titel der Seite

Subtitel
-
```

Begründung:
* Die Überschrift sticht besser in einem Texteditor hervor


Unnummerierte Aufzählungen
--------------------------

Für nicht numerierte Aufzählungen ist nur der `*` erlaubt.
Unterpunkte sind mit 2 Leerzeichen einzurücken.

Postitiv Beispiel:
```
* Liste von Dingen
  * Unterpunkt
    * Unter-Unterpunkt
* Noch ein Punkt
```

Negativ Beispiel:
```
- Liste von Dingen
  - Unterpunkt
    - Unter-Unterpunkt
- Noch ein Punkt
```

Begründung:
* Die Darstellung im Texteditor ähnelt mehr der realen Darstellung


Nummerierte Aufzählungen
------------------------

Nummerierte Aufzählungen sollten auch korrekt im Markdown durchnummeriert werden.
Unterpunkte sind mit 3 Leerzeichen einzurücken.

Positiv Beispiel:
```
1. Computer an Strom anschließen
2. Ubuntu installieren
   1. Stick einstecken
   2. Computer starten
3. System testen
```

Negativ Beispiel (hier wird die Aufzählung erst bei der Darstellung durchnummeriert):
```
1. Computer an Strom anschließen
1. Ubuntu installieren
  1. Stick einstecken
  1. Computer starten
1. System testen
```

Begründung:
* Der Vorteil einer nummerierten Aufzählung ist die Referenzierung von Punkten (gehe zu Schritt 2).
  Wenn dies im Markdown-Dokument fehlt wird ein Lesen mit einem Texteditor erschwert


Hyperlinks
----------

Für Hyperlinks außerhalb der Dokumentation muss - soweit die Webseite es anbietet - https genutzt werden.
Für Hyperlinks innerhalb der Dokumentation darf nicht auf Parent-Ordner verwiesen werden (d.h. kein `..`).
Sollte der Hyperlink sich auf das aktuelle Dokument beziehen, so darf lediglich der Anchor genutzt werden, aber
nicht der aktuelle Dateiname.
Hyperlinks innerhalb der Dokumentation sollten denselben Namen tragen, wie die erste Überschrift der entsprechenden
Dokumentationsseite.

Positiv Beispiel:
```
Das ist [GitLab](https://gitlab.com/)
Siehe [Subsektion](#Subsektion)
```

Negativ Beispiel:
```
Das ist [GitLab](http://gitlab.com/)
Hier ein Parent-Link: [Parent](../README.md)
Wie wärs mit ner [Subsektion](meinedatei.md#Subsektion)
Die Seite heißt zwar Rechner aufsetzen, aber hier ist [Aufsetzen](rechner-aufsetzen.md)
```

Begründung:
* HTTPS ist eine verschlüsselte Übertragung und sollte daher nach Möglichkeit verwendet werden
* Parent-Links erwarten eine vollständige und konsistente Ordnerstruktur.
  Dies erschwert Änderungen, wie bsp. das Umbenennen (da quasi überall was kaputtgehen könnte)
* Ohne Parent-Links können problemlos einzelne Teilordner in bsp. andere Repositories verschoben werden.


Dateinamen in Englisch
----------------------

Alle Dateinamen sollten in Englisch geschrieben sein.

Begründung:
* Vermeidung von Enkodierungsproblemen bei der Nutzung von Umlauten
* Einige Programmen erwarten bestimmte Dateinamen, welche ebenfalls in englischer Sprache sind (bsp. `README`, `COPYING`, `.editorconfig`)
